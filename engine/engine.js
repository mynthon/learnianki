lhelp.lib.engine = class {

    /**
     * 
     * @param {*} htmlContainerId 
     * @param {*} options 
     */
    constructor (htmlContainerId, options) {

        /**
         * List of questions available
         */
        this.questions = [];

        /**
         * Indexes of questions to ask.
         */
        this.currentSet = [];

        /**
         * Default wage for question
         */
        this.defaultWage = 100;

        /**
         * Value by user. How many *unique* questions should we ask. This is value given by user
         * and is used at init and display reasons, thats why it can't be changed
         */
        this.uniqueToAskNum = 0;

        /**
         * Value internal. How many *unique* questions should we ask. At initiation of new set it
         * is equal to this.uniqueToAskNum, and later is changed internally.
         */
        this.internalUniqueToAskNum = 0; // ustawiane automatycznie

        /**
         * lista unikalnych id zadanych pytań w danej pętli. Służy do sprawdzania czy już
         * zostało zadana odpowiednia liczba pytań zgodna z konfiguracją
         */
        this.askedIndexes = [];
        this.incorectAnswerIndexes = [];

        this.htmlContainer = null;
        this.learnassistMainHtmlContainer = null;

        this.setName = '';

        this.statistics = {
            totalQuestions: 0,
            totalCorrect: 0,
            totalIncorrect: 0
        };

        this.sessionStatistics = {
            questions: 0,
            correct: 0,
            incorrect: 0
        };

        /**
         * 
         * @param {*} engine 
         * @param {*} question 
         * @param {*} correct 
         */
        this.onPostAnswer = function(engine, question, correct){
            // noop
        };

        ///////////////// end of initialization
        
        this.x=Math.round(Math.random() * 100);
        this.htmlContainer = document.getElementById(htmlContainerId);
        this.runSetup(options)
        this.setup(options);
  
    }

    /**
     * 
     */
    runSetup (options) {
        this.buildFrame();
    }

    /**
     * 
     * @param {*} options 
     */
    setup (options) {
        
        if (typeof(options) === 'object') {
            if (options.uniqueToAskNum) { this.uniqueToAskNum = options.uniqueToAskNum; }
            if (options.internalUniqueToAskNum) { this.internalUniqueToAskNum = options.internalUniqueToAskNum; }
            if (options.setName) { this.setName = options.setName; }
            if (options.currentSet) { this.currentSet = options.currentSet; }
            if (options.defaultWage) { this.defaultWage = options.defaultWage; }
            if (options.askedIndexes) { this.askedIndexes = options.askedIndexes; }
            if (options.incorectAnswerIndexes) { this.incorectAnswerIndexes = options.incorectAnswerIndexes; }
            if (options.statistics) { this.statistics = options.statistics; }

            if (options.questions) { 
                var qf = new lhelp.lib.questionFactory(this);
                var q = qf.massCreate(options.questions);
                for (var i=0; i<q.length; i++) {
                    this.addQuestion(q[i]);
                }

            }
        }
 
        for(var i=0; i<this.questions.length; i++) {
            this.currentSet[i] = this.questions[i].questionIndex;
        }
    }

    /**
     * 
     */
    buildFrame () {
        var html = '' +
            '<div id="learnassistMainHtmlContainer" class="' + Math.round(Math.random() * 100) + '"></div>';

        this.htmlContainer.innerHTML = html;
        this.learnassistMainHtmlContainer = document.getElementById('learnassistMainHtmlContainer');
    }

    destroyFrame () {
        document.getElementById('learnassistMainHtmlContainer').remove();
    }

    /**
     * 
     * @param {*} question 
     */
    addQuestion (question) {
        if (question.currentWage < 1) { question.currentWage = this.defaultWage; }
        if (question.engine === null) { question.engine = this; }

        question.questionIndex = this.questions.length;
        this.questions.push(question);
    }

    /**
     * 
     */
    renderNext () {
        this.learnassistMainHtmlContainer.innerHTML = '';
        var next = this.getNextQuestion();
        if (next===null && this.currentSet.length===0) {
            alert('ostatnie?')
        } else {
            next.render(this.learnassistMainHtmlContainer);
        }
    }

    /**
     * Metoda zwraca kolejne pytanie. Jeśli nie ma już pytań do zadania metoda zwraca null
     * 
     * @returns mixed
     */
    getNextQuestion () {
        var infiniteAsking = false;
        if (isNaN(this.uniqueToAskNum) || this.uniqueToAskNum < 1 || this.uniqueToAskNum >= this.currentSet.length ) {
            infiniteAsking = true;
        } 

        // na początku wypełniamy currentSet wszystkimi pytaniami. Niezależnie czy to ma być dla infinitive
        // czy nie dla infinitive. 
        if ( this.askedIndexes.length === 0 && this.questions.length !== 0 ) {
            for (var i=0; i<this.questions.length; i++) {
                this.currentSet.push(this.questions[i].questionIndex);
            }
            this.internalUniqueToAskNum = Math.min(this.uniqueToAskNum, this.currentSet.length);
        }

        // jesli mamy infinitive asking to pytamy w kółko, cały czas wybierajac dowolne pytania z całego setu.
        // w takim przypadku currentSet jest całym setem i go nigdy nie modyfikujemy
        // jeśli nie pytamy w nieskończoność to po przekroczeniu liczby zadanych pytań wypełniamy set na nowo 
        // ale tylko pytaniami źle odpowiedzianymi.
        if ( ! infiniteAsking ) {
            if ( this.askedIndexes.length >= this.internalUniqueToAskNum ) {
                this.currentSet = [];
                for (var i=0; i<this.incorectAnswerIndexes; i++) {
                    this.currentSet.push(this.questions[this.incorectAnswerIndexes[i]].questionIndex)
                }
                this.incorectAnswerIndexes = [];
                this.askedIndexes = [];
                this.internalUniqueToAskNum = this.currentSet.length;
            }
        }

        // Nie ma już pytań do zadania.
        if ( this.currentSet.length === 0) {
            var endPage = new lhelp.lib.endPage(this);
            endPage.render();
            return null;
        }

        var currentTotalWage = 0;
        for (var i=0; i<this.currentSet.length; i++) {
            currentTotalWage += this.questions[this.currentSet[i]].currentWage;
        }

        var expectedItemWage = this.getRandomInt(currentTotalWage);
        var currentItemWage = 0;
        var toReturn = null;

        for (var i=0; i<this.currentSet.length; i++) {
            currentItemWage = currentItemWage + this.questions[this.currentSet[i]].currentWage;
            if (currentItemWage > expectedItemWage) {
                toReturn = this.questions[this.currentSet[i]];
                break;
            }
        }

        // nie pytamy w nieskończoność? Dodaj obecne pytanie do indeksu już zadanych
        if ( ! infiniteAsking ) {
            if (this.askedIndexes.indexOf(toReturn.questionIndex) === -1 ) {
                this.askedIndexes.push(toReturn.questionIndex);
            } 
        }

        return toReturn;
    }

    /**
     * 
     * @param {*} question 
     * @param {*} correct 
     */
    postAnswer (question, correct) {
        this.statistics.totalQuestions++;
        this.sessionStatistics.questions++;
        if (correct) {
            this.statistics.totalCorrect++;
            this.sessionStatistics.correct++;
        } else {
            this.incorectAnswerIndexes.push(question.questionIndex);
            this.statistics.totalIncorrect++;
            this.sessionStatistics.incorrect++;
        }

        this.onPostAnswer(this, question, correct);
    }

    /**
     * 
     */
    toJSON () {
        var j = {};
    
        j.questions = [];
        j.setName = this.setName;
        j.currentSet = this.currentSet;
        j.defaultWage = this.defaultWage;
        j.uniqueToAskNum = this.uniqueToAskNum;
        j.internalUniqueToAskNum = this.internalUniqueToAskNum;
        j.askedIndexes = this.askedIndexes;
        j.incorectAnswerIndexes = this.incorectAnswerIndexes;
        j.statistics = this.statistics;

        for (var i = 0; i< this.questions.length; i++) {
            j.questions.push(this.questions[i].toJSON());
        }
    
        return j;
    }

    /**
     * 
     * @param integer maxRandomInt 
     */
    getRandomInt (maxRandomInt) {
        return Math.floor(Math.random() * maxRandomInt);
    }

}


/**
 * Strona startowa
 */
lhelp.lib.startPage = class {
    constructor(engine) {
        this.engine = engine;
    }

    render() {

        var html = '' +
        '<button id="firsttestpage-startbtn" type="button">Start</button>';

        this.engine.learnassistMainHtmlContainer.innerHTML = html;

        document.getElementById('firsttestpage-startbtn').addEventListener('click', function(){
            this.engine.renderNext();
        }.bind(this))
    }
}


/**
 * 
 */
lhelp.lib.endPage = class {
    constructor(engine) {
        this.engine = engine;
    }

    render(){
        this.engine.learnassistMainHtmlContainer.innerHTML = 'koniec';
    }
}


/**
 * 
 */
lhelp.lib.questionFactory = class {
    constructor(app){
        this.app = app;
    }

    massCreate(questionsJson) {
        var questions = [];
        for(var i=0; i<questionsJson.length; i++) {
            var question = null;
            if (['exact', 'honest', 'testSingle'].indexOf(questionsJson[i].className) !== -1) {
                question = new lhelp.lib.questions[questionsJson[i].className](this.app, questionsJson[i]);
                questions.push(question);
            }
        }
        return questions;
    }
}
