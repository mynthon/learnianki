lhelp.lib.questions.testSingle = class extends lhelp.lib.questions.abstract {

    constructor(engine, options) {
        super(engine, options);
        this.className = 'testSingle';

        this.validAnswer = 0;
        this.availableAnswers = [];
        this.question = 'choose';
        this.randomOrder = false;

        if (typeof(options) === 'object') {
            if (options.validAnswer) { this.validAnswer = options.validAnswer; }
            if (options.question) { this.question = options.question; }
            if (options.availableAnswers) { this.availableAnswers = options.availableAnswers; }
            if (options.randomOrder) { this.randomOrder = options.randomOrder; }
        }
    }

    /**
     * 
     * @param {*} target 
     */
    render (target) {
        super.render(target);

        var qarr = [];
        for(var i=0; i<this.availableAnswers.length; i++) {
            qarr.push({'i':i, 'q':this.availableAnswers[i]});
        }

        if (this.randomOrder){
            var j, x, i;
            for (i = qarr.length - 1; i > 0; i--) {
                j = Math.floor(Math.random() * (i + 1));
                x = qarr[i];
                qarr[i] = aqarr[j];
                qarr[j] = x;
            }
        }

        var container = document.createElement('section');
        var radiosString = '';
        for(var i=0; i<qarr.length; i++) {
            radiosString += '<label><input type="radio" name="testsingle-select-answer" value="' + qarr[i].i + '" class="testsingle-radiooption" />' + qarr[i].q + '</label>';
        }

        var html = this.getStatsHtml() +
            '<article id="learnassist-testsingle-type-question" class="learnassist-question-container">'
            (this.title ? '<h1>' + this.title + '</h1>' : '') +
            '<div>' + this.question + '</div>' +
            '<section class="selects">' + radiosString + '</section>'

        html += '<p class="testsingle-valid-answer" style="display:none;">Tak, "<span class="testsingle-current-answer"></span>" to jest poprawna odpowedź.</p>';
        html += '<p class="testsingle-invalid-answer" style="display:none;">Niestety, "<span class="testsingle-current-answer"></span>" to nie jest poprawna odpowedź. <br />' +
                'Poprawna odpowiedź to "' + this.availableAnswers[this.validAnswer] + '".</p>';
        html += '<button type="button" class="testsingle-validate">Odpowiedz</button>';
        html += '<div class="testsingle-next-section" style="display:none;"><button type="button" class="testsingle-next">Następne</button></div>';
        html += '</article>';

        container.innerHTML = html;
        target.appendChild(container)

        var validBtn = document.querySelector('.testsingle-validate').addEventListener('click', function(){
            var opts = document.querySelectorAll('.testsingle-radiooption');
            var isValid = null;
            var currentText = '';
            for(var i=0; i<opts.length; i++) {
                if (opts[i].checked){
                    var idx = parseInt(opts[i].value);
                    currentText = this.availableAnswers[idx];
                    if (idx === this.validAnswer) {
                        isValid = true;
                    } else {
                        isValid = false;
                    }

                    break;
                }
            }

            if (isValid !== null) {
                var toChange = document.querySelectorAll('.testsingle-current-answer');
                for (var i = 0; i < toChange.length; i++) {
                    toChange[i].innerHTML = currentText;
                }

                if (isValid){
                    document.querySelector('.testsingle-valid-answer').style.display = 'block';
                } else {
                    document.querySelector('.testsingle-invalid-answer').style.display = 'block';
                }

                
                document.querySelector('.testsingle-next-section').style.display = 'block';
                document.querySelector('.testsingle-validate').style.display = 'none';

                this.answer(isValid);
            } else {
                alert('Wybierz jedną z odpowiedzi.')
            }

  
        }.bind(this));

        // render next question
        document.querySelector('.testsingle-next').addEventListener('click', function(){
            this.engine.renderNext();
        }.bind(this))

    }

    answer (answer){
        this.calculateWage(answer);
        this.postAnswer(answer);
    }

    toJSON() {
        var j = super.toJSON();

        j.validAnswer = this.validAnswer;
        j.availableAnswers = this.availableAnswers;
        j.question = this.question;
        j.randomOrder = this.randomOrder;

        return j
    }
}
