lhelp.lib.questions.honest = class extends lhelp.lib.questions.abstract {

    constructor(engine, options) {
        super(engine, options);
        this.className = 'honest';

        this.validAnswer = 'kukurydzianki';
        this.question = 'Jaki kuk rydz i anki';

        if (typeof(options) === 'object') {
            if (options.validAnswer) { this.validAnswer = options.validAnswer; }
            if (options.question) { this.question = options.question; }
        }
    }

    /**
     * 
     * @param {*} target 
     */
    render (target) {
        super.render(target);

        var container = document.createElement('section');
        container.innerHTML = this.getStatsHtml() + 
            '<article id="learnassist-honest-type-question" class="learnassist-question-container">' +
                (this.title ? '<h1>' + this.title + '</h1>' : '') + 
                '<div title="question-text">' + this.question + '</div>' + 
                '<button class="show-honest-answer">Pokaż odpowiedź</button>' + 
                '<div class="answer-rating" style="display:none">' +
                    '<div class="valid-answer">Prawidłowa odpowiedź to: ' + this.validAnswer + '</div>' +
                    '<div class="notice">Jak oceniasz swoją dopowiedź? Wybierz jedną z opcji. Pamiętaj, żeby oceniać siebie uczciwie.</div>' + 
                    '<div class="buttons">' +
                        '<button class="honest-answer-wrong wrong">Zła</button>' +
                        '<button class="honest-answer-hard hard">Dobrze, ale musiałem(am) dłużej pomysleć</button>' + 
                        '<button class="honest-answer-good good">Dobrze, odpowiedziałem(am) szybko</button>' + 
                        '<button class="honest-answer-perfect perfect">Perfekt!</button>' + 
                    '</div>' +
                '</div>' +
            '</article>';
        target.appendChild(container)

        var showBtn = document.querySelector('.show-honest-answer');
        showBtn.addEventListener('click', function(){
            document.querySelector('#learnassist-honest-type-question .answer-rating').style.display = 'block';
            document.querySelector('.show-honest-answer').style.display = 'none';
        }.bind(this));


        document.querySelector('.honest-answer-wrong').addEventListener('click', function(){
            this.answer(0);
            this.engine.renderNext();
        }.bind(this));

        document.querySelector('.honest-answer-hard').addEventListener('click', function(){
            this.answer(1);
            this.engine.renderNext();
        }.bind(this));

        document.querySelector('.honest-answer-good').addEventListener('click', function(){
            this.answer(2);
            this.engine.renderNext();
        }.bind(this));

        document.querySelector('.honest-answer-perfect').addEventListener('click', function(){
            this.answer(3);
            this.engine.renderNext();
        }.bind(this));



    }

    answer (answer){
        switch (answer) {
            case 0:
                this.calculateWage(false);
                this.postAnswer(false);
                break;
            case 1:
                //this.calculateWage(isValid); nie zmieniaj wagi
                this.postAnswer(true);
                break;
            case 2:
                this.calculateWage(true);
                this.postAnswer(true);
                break;
            case 3:
                this.calculateWage(true);
                this.calculateWage(true);
                this.postAnswer(true);
                break;
        }
    }

    toJSON() {
        var j = super.toJSON();

        j.validAnswer = this.validAnswer;
        j.question = this.question;

        return j;
    }
}
