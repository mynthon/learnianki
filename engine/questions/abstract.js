lhelp.lib.questions.abstract = class {

    constructor (engine, options) {
        this.className = '';
        this.questionIndex = 0;

        if (engine) { this.engine = engine; } else { this.engine = null; }

        this.maxWage = 1000;
        this.minWage = 10;
        this.defaultWage = 100;

        this.title = '';
        this.currentWage = 100;
        this.successes = 0;
        this.fails = 0;
        this.lastWasSuccess = false;
        
        if (typeof(options) === 'object') {
            if (options.title) { this.title = options.title; }
            if (options.currentWage) { this.currentWage = options.currentWage; }
            if (options.successes) { this.successes = options.successes; }
            if (options.fails) { this.fails = options.fails; }
            if (options.lastWasSuccess) { this.lastWasSuccess = options.lastWasSuccess; }
            if ( !options.currentWage && this.successes < this.fails) { this.currentWage = this.currentWage * 2; }
        }
    }

    calculateWage(correct) {
        var oneTenth = Math.round(this.defaultWage / 10);
        var wageStepUp = Math.ceil( ((this.maxWage - this.currentWage) * 1.4) / 100 ) * oneTenth;




        if ( correct ) {
            if ( this.currentWage > this.defaultWage && this.currentWage < this.defaultWage*3 ){              // 100 < x < 300
                this.currentWage = this.defaultWage;
            } else if ( this.currentWage <= this.defaultWage/2) {                                             // x <= 50
                this.currentWage -= oneTenth;
            } else if ( this.currentWage > this.defaultWage/2 && this.currentWage <= this.defaultWage ) {     // 50 < x <=100
                this.currentWage = Math.round(this.defaultWage/2);
            } else if ( this.currentWage > this.defaultWage && this.currentWage < this.defaultWage*5) {       // 100 < x < 500
                this.currentWage = Math.round(math.currentWage - this.defaultWage*2);
            } else {                                                                                          // x <= 1000
                this.currentWage = Math.round(math.currentWage - this.defaultWage*2);
            }
        } else {
            if ( this.currentWage > this.defaultWage && this.currentWage < this.defaultWage*3 ){              // 100 < x < 300
                this.currentWage = this.defaultWage;
            } else if ( this.currentWage <= this.defaultWage/2) {                                             // x <= 50
                this.currentWage = Math.round(this.defaultWage/2);
            } else if ( this.currentWage > this.defaultWage/2 && this.currentWage <= this.defaultWage ) {     // 50 < x <=100
                this.currentWage = Math.round(math.currentWage + this.defaultWage*2);
            } else if ( this.currentWage > this.defaultWage && this.currentWage < this.defaultWage*5) {       // 100 < x < 500
                this.currentWage = Math.round(math.currentWage + this.defaultWage*2);
            } else {                                                                                          // x <= 1000
                this.currentWage = Math.round(math.currentWage + this.defaultWage*2);
            }
        }






        /*if (this.currentWage > this.defaultWage) {
            var wageStepDown = Math.ceil ( (this.currentWage - this.defaultWage) / 100 ) * oneTenth;
        } else {
            var wageStepDown = Math.ceil ( this.currentWage / oneTenth / 2) * oneTenth;
        }
        
        if(correct && this.currentWage > this.minWage){
            this.currentWage = this.currentWage - wageStepDown;
        } else if ( !correct && this.currentWage < this.maxWage) {
            this.currentWage = this.currentWage + wageStepUp;
        }*/

        if (this.successes < this.fails) {
            this.currentWage = this.currentWage + Math.ceil((this.fails - this.successes) / 2) * oneTenth;
        }

        if (this.currentWage < this.minWage) {
            this.currentWage = this.minWage;
        }

        if (this.currentWage > this.maxWage){
            this.currentWage = this.maxWage;
        }

        return this;
    }

    postAnswer(correct) {
        if ( correct ) {
            this.successes++;
            this.lastWasSuccess = true;
        } else {
            this.fails++;
            this.lastWasSuccess = false;
        }

        if (this.engine){
            this.engine.postAnswer(this, correct);
        }
    }

    render(target) {
        console.log('Implement render()');
        //if (!target) { target = document.querySelector('body'); }
        target.innerHTML = '';
    }

    getStatsHtml() {
        var html = '<section class="learnassist-question-globalstat">';//<!--<span class="showhide">&#128065;</span>-->

        if (this.engine) {
            html += 'Test:  <span class="setname">' + this.engine.setName + '</span>. '
            html += 'Ta sesja: ' + this.engine.sessionStatistics.questions + '/' + 
                    (this.engine.uniqueToAskNum > 0 ? this.enngine.uniqueToAskNum : '∞') + ' zadanych, ' + 
                    '<span class="correct">+' + this.engine.sessionStatistics.correct + '</span>/' +
                    '<span class="incorrect">-' + this.engine.sessionStatistics.incorrect + '</span>'
            html += ' | Ogólnie: ' + this.engine.statistics.totalQuestions + ' zadanych, ' + 
                    '<span class="correct">+' + this.engine.statistics.totalCorrect + '</span>/' +
                    '<span class="incorrect">-' + this.engine.statistics.totalIncorrect + '</span>'
        }

        html += '</section>';
        return html;
    }

    answer() {
        console.log('Implement answer()');
    }

    toJSON() {
        var j = {
            'className': this.className,
            'title': this.title,
            'currentWage': this.currentWage,
            'successes': this.successes,
            'fails': this.fails,
            'lastWasSuccess': this.lastWasSuccess
        };
        
        return j;
    }
}