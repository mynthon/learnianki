lhelp.lib.questions.exact = class extends lhelp.lib.questions.abstract {
    
    constructor (engine, options) {
        super(engine, options);
        this.className = 'exact';

        this.validAnswer = 'koko';
        this.question = '';
        this.caseSensitive = true;
        this.ifValidTemplate = 'Super!';
        this.ifInvalidTemplate = 'Ups. Poprawna odpowiedź to: "{%valid_answer%}"'; // {%given_answer%} {%valid_answer%}
        
        this.lastAnswer = '';
        this.htmlContainer = null;

        if (typeof(options) === 'object') {
            if (options.validAnswer) { this.validAnswer = options.validAnswer; }
            if (options.caseSensitive) { this.caseSensitive = this.caseSensitive; }
        }
    }
    
    render(target) {
        super.render(target);

        if (!target) { target = document.querySelector('body'); }
        this.htmlContainer = target;

        var container = document.createElement('section');
        container.innerHTML = this.getStatsHtml() + 
            '<article id="learnassist-exact-type-question">' + 
                (this.title ? '<h1>' + this.title + '</h1>' : '') + 
                '<div title="question-text">' + this.question + '</div>' + 
                '<div class="input-box">' + 
                    '<input type="text" id="exact-answer-questionanswer" />' +
                    '<button type="button" id="exact-answer-answerbtn">Odpowiedz</span>' + 
                '</div>' +
                '<div class="answer-container"></div>' +
                '<div class="exact-answer-next-section" style="display:none;">' +
                    '<button type="button" class="exact-answer-next">Następne</button>' +
                '</div>' +
            '</article>';
        target.appendChild(container)

        // events
        document.getElementById('exact-answer-answerbtn').addEventListener('click', function(){
            this.answer(document.getElementById('exact-answer-questionanswer').value);
        }.bind(this));

        // events
        document.getElementById('exact-answer-questionanswer').focus();
        document.getElementById('exact-answer-questionanswer').addEventListener('keypress', function(e){
            if ( (e.keyCode ? e.keyCode : e.key)===13 ){
                this.answer(document.getElementById('exact-answer-questionanswer').value);
            }
        }.bind(this));

        // render next question
        document.querySelector('#learnassist-exact-type-question .exact-answer-next').addEventListener('click', function(){
            this.engine.renderNext();
        }.bind(this))
    }

    renderValidAnswer() {
        var text = this.ifValidTemplate.replace('{%given_answer%}', this.lastAnswer).replace('{%valid_answer%}', this.validAnswer);
        this.htmlContainer.querySelector('#learnassist-exact-type-question .answer-container').innerHTML = '' + 
            '<div class="correct">' + text + '</div>';
        this.htmlContainer.querySelector('.exact-answer-next-section').style.display = 'block';
    }

    renderInvalidAnswer() {
        var text = this.ifInvalidTemplate.replace('{%given_answer%}', this.lastAnswer).replace('{%valid_answer%}', this.validAnswer);
        this.htmlContainer.querySelector('#learnassist-exact-type-question .answer-container').innerHTML = '' +
            '<div class="incorrect">' + text + '</div>';
        this.htmlContainer.querySelector('.exact-answer-next-section').style.display = 'block';
    }

    /**
     * 
     * @param {*} answer 
     */
    answer(answer){
        if (document.getElementById('exact-answer-questionanswer').value === '') {
            alert("Podaj odpowiedź");
            document.getElementById('exact-answer-questionanswer').focus();
            return false;
        } 


        document.querySelector('#learnassist-exact-type-question .input-box input').disabled='disabled';


        this.lastAnswer = answer;

        if (this.caseSensitive) { 
            var isValid = (answer === this.validAnswer);
        } else {
            var isValid = (answer.toLowerCase() === this.validAnswer.toLowerCase());
        }
        

        if (isValid){
            this.renderValidAnswer();
        } else {
            this.renderInvalidAnswer();
        }

        this.calculateWage(isValid);
        this.postAnswer(isValid);
    }

    toJSON() {
        var j = super.toJSON();

        j.validAnswer = this.validAnswer;
        j.caseSensitive = this.caseSensitive;
        j.ifValidTemplate = this.ifValidTemplate;
        j.ifInvalidTemplate = this.ifInvalidTemplate;

        return j;
    }
}
