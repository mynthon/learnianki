var exampleSet = {
    'setName': 'set testowy',
    'questions':[
        {
            'className': 'honest',
            'title':'a1-1', 
            'question': 'test question', 
            'validAnswer': 'test valid answer'
        },
        {
            'className': 'honest',
            'title':'a1-2'
        },
        {
            'className': 'testSingle',
            'title':'a1-3',
            'availableAnswers': ['nan1', 'nan2(v)', 'nan3', 'nan4'],
            'validAnswer': 1
        },
        {
            'className': 'testSingle',
            'title':'a1-4',
            'availableAnswers': ['a', 'b', 'c'],
            'validAnswer': 1
        },
        {
            'className': 'exact',
            'title':'a1-5'
        },
        {
            'className': 'exact',
            'title':'a1-6'
            
        },
        {
            'className': 'exact',
            'title':'a1-7'
        },
        {
            'className': 'exact',
            'title':'a1-8'
        }
    ]
};





var SetsLoader = {
    sets: [],
    currentSet: null,
    currentSetIndex: null,
    localStorageMap: [],

    target: null,

    addSet: function(setJson){
        this.sets.push(setJson);
    },

    createScreen: function(targetId) {
        this.target = document.getElementById(targetId);

        var selectOptions = '<option value="">Wybierz</option>';

        for(var i=0; i<this.sets.length; i++) {
            selectOptions += '<option value="' + i + '">' + this.sets[i].setName + '</option>'
        }

        var currentStorageIndex = 0;
        for(var k in window.localStorage){
            if (k.indexOf('[learnhelper-setObject]') === 0){
                if (currentStorageIndex === 0) {
                    selectOptions += '<option value="">-- zapisane zestawy --</option>';
                }
                
                var temp = JSON.parse(window.localStorage[k])
                selectOptions += '<option value="' + currentStorageIndex + '_">' + temp.setName + '</option>';
                this.localStorageMap[currentStorageIndex] = k;
                currentStorageIndex++
            }
        }

        var html = '<section id="setsloadermainpanel">' +
            '<div id="options">' +
                '<button type="button" id="setsloadersetsbutton">Ekran wyboru testu</button>' + 
                '<button type="button" id="setsloadersavebutton">Zapisz</button>' +
            '</div>' + 
            '<section id="setsloaderselect">' +
            '<select>' + selectOptions + '</select>' +
            ' twój nick: <input type="text" name="uname" class="uname" />' +
            '<button type="button" class="load">ok</button>' + 
            '</section>' +
            '</section>';

        this.target.innerHTML = html;
        
        //
        //on select
        document.getElementById('setsloaderselect').querySelector('select').addEventListener('change', function(){
            var select = this.target.querySelector('select')
            var setId = select[select.selectedIndex].value;
            if ( (/^[0-9]+_$/.test(setId)) ) {
                var temp = JSON.parse(window.localStorage[this.localStorageMap[parseInt(setId)]]);
                //var uname = temp.setName.match(/^\[.*?\]\((.*?)\)/)[1];
                var uname = temp.setName.match(/^\((.*?)\)/)[1];
                this.target.querySelector('.uname').value = uname;
            }

        }.bind(this));

        //
        // on save click
        var saveButton = document.querySelector('#setsloadermainpanel #setsloadersavebutton');
        saveButton.addEventListener('click', function(button){

            if ( ! button.disabled){ return false; }

            if (this.currentSet) {
                this.save(lhelp.app.toJSON());
            }

        }.bind(this, saveButton));

        //
        // on set choose click
        var loadButton = document.getElementById('setsloadermainpanel').querySelector('#setsloadersetsbutton');
        loadButton.addEventListener('click', function(button){
            if ( ! button.disabled){ return false; }

            if (confirm('Chcesz zakończyć obecny test i wybrać inny?')) {
                if (this.currentSet) {
                    this.save(lhelp.app.toJSON());
                    lhelp.app.destroyFrame();
                }
                this.target.querySelector('#setsloaderselect').style.display = 'block';
                //document.querySelector('#learncontainer').innerHTML = '';
            } else {

            }
        }.bind(this, loadButton));

        //
        // on set loader click
        document.getElementById('setsloaderselect').querySelector('button.load').addEventListener('click', function(){
            var select = this.target.querySelector('select')

            var setId = select[select.selectedIndex].value;
            var uname = this.target.querySelector('.uname').value;

            // if set 34_ weź z local storage

            var isFromStorage = false;
            if ( (/^[0-9]+_$/.test(setId)) ) {
                isFromStorage = true;
                setId = setId.substring(0, setId.length - 1);
            } else {
                isFromStorage = false;
            }


            if (setId !== '' && uname !== '' && /^[a-zA-Z0-9_][a-zA-Z0-9 _]+$/.test(uname)) {
                this.currentSetIndex = parseInt(setId)
                

                if ( !isFromStorage ) {
                    this.currentSet = this.sets[this.currentSetIndex]
                    this.currentSet.setName = '(' + uname + ') ' + this.currentSet.setName;
                    
                } else {
                    this.currentSet = JSON.parse(window.localStorage[this.localStorageMap[setId]]);
                }

                lhelp.app = new lhelp.lib.engine(
                    'learncontainer', 
                    this.currentSet
                );
                var startPage = new lhelp.lib.startPage(lhelp.app);
                startPage.render();

                this.target.querySelector('#setsloaderselect').style.display = 'none';
            } else {
                alert('Podaj swój pseudonim i wybierz zestaw. Pseudonim może zawierać tylko litery i cyfry oraz znak "_".')
            }

        }.bind(this))
    },

    /**
     * Zapisuje setObject do local storage
     * 
     * @param object setObject 
     */
    save: function(setObject) {
        console.log(setObject)
        window.localStorage['[learnhelper-setObject]' + setObject.setName] = JSON.stringify(setObject);
        return this;
    }
}

window.onload = function(){
    SetsLoader.addSet(exampleSet);
    SetsLoader.createScreen('loadercontainer');
}

