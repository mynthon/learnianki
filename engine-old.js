var engine = {
    'defaultWage': 32,

    'letters': [
        {'big': 'ABCDEFGHIJK','small': 'abcdefghijk'},
        {'big': 'LMNOPRSTUWZ','small': 'lmnoprstuwz'},
        {'big': 'ĆŁÓŚŻ','small': 'ćłóśż'},
        {'big': 'YĄĘŃŹ','small': 'yąęńź'}
    ],


    'statistics': {},

    init: function(){
        this.createStatistics();
    },

    'createStatistics': function() {
        var bigLetters = '';
        var smallLetters = '';

        for (var i = 0; i < this.letters.length; i++) {
            bigLetters += this.letters[i].big;
            smallLetters += this.letters[i].small;
        }


        var allLetters = (bigLetters + smallLetters).split('')
        var defaultWage = this.defaultWage;

        this.statistics.letters = [];

        for (var i = 0; i < allLetters.length; i++) {
            var isBig = (i < bigLetters.length) // allLetters = big + small. all indexes < big.length are big.
            var index = this.statistics.letters.length;
            this.statistics.letters.push({
                'index': index,
                'wage': defaultWage,
                'name': allLetters[i],
                'success': 0,
                'fail': 0,
                'isBig': isBig
            });
        }

        this.statistics.currentSet = []; // tis holds only index in global tab and item wage
        this.statistics.currentTotalWage = 0;
    },

    prepareLettersetIndexes: function (maxDifficultyLevel) {
        if (maxDifficultyLevel > this.letters.length) {
            maxDifficultyLevel = this.letters.length;
        }

        var bigs = '';
        var smalls = '';
        var indexed = [];

        for (var i = 0; i < maxDifficultyLevel; i++) {
            bigs += this.letters[i]['big'];
            smalls += this.letters[i]['small'];
        }

        var combined = bigs + smalls;

        for (var j=0; j<this.statistics.letters.length; j++) {
            var lindex = combined.indexOf(this.statistics.letters[j].name);
            if (lindex > -1) {
                indexed.push({
                    'index': j,
                    'totalWage': 0 // cumulative wage for this item. non cumulative is hold in item data.
                });
            }
        }

        this.statistics.currentSet = indexed;
    },

    getRandomInt: function(x){
        return Math.floor(Math.random() * x);
    },

    /**
     * 
     */
    updateTotalWage: function() {
        var totalWage = 0;
        for (var i=0; i<this.statistics.currentSet.length; i++) {
            var cset = this.statistics.currentSet[i];
            totalWage += this.statistics.letters[cset.index].wage;
            this.statistics.currentSet[i].totalWage = totalWage;
        }
        this.statistics.currentTotalWage = totalWage;
    },

    /**
     * Choose 5 letters from all available
     */
    chooseLetters: function(){
        this.updateTotalWage();

        var letterIndexes = [];
        var debug = [];

        for (var i = 0; i < 5; i++) {
            var rnd = this.getRandomInt(this.statistics.currentTotalWage);
            for (var l = 0; l < this.statistics.currentSet.length; l++){

                var setData = this.statistics.currentSet[l];
                var letter = this.statistics.letters[setData.index];

                if (rnd < setData.totalWage) {
                    if (letterIndexes.indexOf(setData.index) === -1){
                        letterIndexes.push(setData.index);
                        debug.push(setData.index + ', ' + letter.name + ', ' + rnd + '@' + setData.totalWage);
                    } else {
                        i--;
                    } 
                    break;
                }
            }
        }

        //return debug;
        return letterIndexes;
    },

    chooseLetterIndexFromSet: function(letterIndexesSet) {
        var rnd = this.getRandomInt(letterIndexesSet.length);
        return letterIndexesSet[rnd];
    }, 

    /**
     * 
     * @param {*} index 
     */
    getDetailsForIndex: function (numIndex){
        return this.statistics.letters[numIndex];
    },

    /**
     * 
     */
    createSoundList: function() {
        // show b/s l [l] like [example]
        var soundList = [
            'show',
            'big/small',
            'l',
            'l-name',
            'like',
            'example'
        ]
    }

}



var audioControl = {
    'audioContext': null,
    'request': null,
    'soundsArray': [],

    init: function(){
        this.audioContext = new (window.AudioContext || window.webkitAudioContext)(); // Create an AudioContext instance for this sound
        this.request = new XMLHttpRequest();
        this.request.responseType = 'arraybuffer';
        this.request.onload = function() {
            // Decode the audio once the require is complete
            this.audioContext.decodeAudioData(this.request.response, 
                function(buffer) {
                    var source; // source is one time object. has to be recreated again for every sound.
                    source = this.audioContext.createBufferSource(); // Create a buffer for the incoming sound content
                    source.connect(this.audioContext.destination); // Connect the audio to source (multiple audio buffers can be connected!)
                    source.loop = false;
                    source.onended = function(){
                        if (this.soundsArray.length === 0){
                            console.log('stop playing');
                        } else {
                            this.playNext();
                        }
                    }.bind(this)
                    source.buffer = buffer;
                    source.start(0); // Play the sound!
                }.bind(this), 
                function(e) {
                    console.log('Audio error! ', e);
                }
            );
        }.bind(this)

    },



    playNext: function(){
        if (this.soundsArray.length > 0) {
            var playNext = this.soundsArray.shift()
            // Set the audio file src here
            this.request.open('GET', playNext, true);
            // Send the request which kicks off 
            this.request.send();
        }
    },

    test: function(){
        this.soundsArray =['choose.mp3', 'big.mp3', 'letter.mp3'];
        this.playNext();
    }



}